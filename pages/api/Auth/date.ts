import axios from "axios";
// import db from '../../../models';
// db.sequelize.sync();
// const Usr = db.user;

export default async function date(req, res) {

    const accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5MjYyYmUzMS02MGQzLTQ4NjktOGFjOS0yNGNiMjYzMDZjOTUiLCJqdGkiOiJjZDgzN2Q4NjgzZGYxOGUzNzY1NWFkOTUwNTU5NGU4OTBiNWM0NWFmY2VkNWM5YjBiOGVlN2YwNjczNGEyYTFkNmMwODczNzI0NzQyNDIwYSIsImlhdCI6IjE2MTA5NTU2NTguMjQ4MTQ2IiwibmJmIjoiMTYxMDk1NTY1OC4yNDgxNTUiLCJleHAiOiIxNjQyNDkxNjU4LjIzMzUxOCIsInN1YiI6IjEiLCJzY29wZXMiOlsiKiJdfQ.vy9ps3I9NxWXB7gv630jyymNN0jiKUydm_7RlFiOS-lB0ERCM6F3tAFA6lK-fKpWkwBj0Q_8PtLhVOL_JfrJMU-9Izd4lHCtlzlpGzXEcfX6_t2C-WIJ6elEBCJj1Q8BijUE8A8F071owoYi8lQN_HxqbsrN6cWWZ7swc4tcO4ETwO4wZ0nc3P_ndf3cDuZ_LK0PityxPJSz1NtRIwNmYD57-9l2PQCbJ_ctn-Nf9Rc9njy-GbMQCaq-c3YuDvOusBHUbG8fQql3iK0831je_m6_65-m3S_NshP91_LVemyxj6nOG1DgDOl9ZnqWIf5vtFavke5H-rxq-r_0MUHFn_d6wy2i-47VdCsBKuKGzArqUO3mTu7kTxV53QZV_eJoKsgF6WloUKQka3HpNHoi_eo46t7z9D4j3Cfv53o9ibSKTJeyssee_d24ZfvxHb1lztO719TU2JuxhGsf7fOnjY1i09s-sVcdjh8MSdQzrgQoA6qatIadsid8tEHxG2qyR4oVncttJZaWr0PJ8M5t7QFgsS5-NmXDrczSOIG2t99nBYCbWQF91X4GN7s0x-LFp0ii-TMiAmpHNktIhTkipi0wGFijiRpqLyzA7ekf3DPFMTad6IVx_nPQu2-NagN8JpJHIkks1x05vsH3kINnxy0naztm-8j2WCJR0wIlVNE'

    // const addUsr = await Usr.build({
    //     userAccessToken: accessToken
    // });
    // await addUsr.save();

    // await Usr.destroy({
    //     where: { userAccessToken: accessToken }
    // })

    // const findUser = await Usr.findOne({
    //     where: { userAccessToken: accessToken }
    // })

    let result = await axios.get(`http://foroosh.net/api/v1/util/today`,
        { headers: { Authorization: `Bearer ${accessToken}` }, })

    return res.json(result.data.data)
}