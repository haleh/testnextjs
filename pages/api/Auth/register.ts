import axios from "axios";
import OATH from "./oath";

export default async function Register(username, req, res) {

    let accessToken = await OATH;

    let result = await axios.post(`http://foroosh.net/api/v1/auth/entry`, {
        username: username
    }, { headers: { Authorization: `Bearer ${accessToken}` }, });

    return res.json(result.data.data);
}