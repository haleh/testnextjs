import Cookies from "universal-cookie";

const getcookies = (cookie: string) => {
    const cookies = new Cookies();
    const got_cookie = cookies.get(cookie);

    return got_cookie;
}

export default getcookies;