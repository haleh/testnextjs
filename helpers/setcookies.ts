import Cookies from "universal-cookie";

const setcookies = (name: string, value: string, path?: string) => {
    const cookies = new Cookies();
    cookies.set(name, value, { path: path });
}

export default setcookies;