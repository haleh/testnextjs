import axios from "axios";

const getfromapi = async (url) => {

    const res = await axios(url)
    const data = await res.data;

    return data;
}

export default getfromapi;