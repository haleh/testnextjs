const Sequelize = require('sequelize');
import userModel from "./user";

const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    operatorsAliases: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    storage: 'db/database.sqlite'
});

const db = {};

db.Sequelize = Sequelize
db.sequelize = sequelize
//...
db.user = userModel(sequelize, Sequelize);

export default db;